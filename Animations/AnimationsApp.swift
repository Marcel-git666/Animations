//
//  AnimationsApp.swift
//  Animations
//
//  Created by Marcel Mravec on 25.01.2022.
//

import SwiftUI

@main
struct AnimationsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
